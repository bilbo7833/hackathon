//
//  HOrderViewController.h
//  Hackathon
//
//  Created by Franz Heinfling on 27.10.13.
//  Copyright (c) 2013 Franz Heinfling. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HOrderViewController : UIViewController <UIPickerViewDataSource, UIPickerViewDelegate>
@property (weak, nonatomic) IBOutlet UIPickerView *topPicker;
@property (weak, nonatomic) IBOutlet UIPickerView *midPicker;
@property (weak, nonatomic) IBOutlet UIPickerView *botPicker;
@property (weak, nonatomic) IBOutlet UIImageView *topImageView;
@property (weak, nonatomic) IBOutlet UIImageView *midImageVIew;
@property (weak, nonatomic) IBOutlet UIImageView *botImageView;
@property (weak, nonatomic) IBOutlet UIButton *orderButton;

@property (nonatomic, strong) UIImage *topImg;
@property (nonatomic, strong) UIImage *midImg;
@property (nonatomic, strong) UIImage *botImg;

@end
