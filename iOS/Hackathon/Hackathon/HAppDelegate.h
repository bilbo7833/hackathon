//
//  HAppDelegate.h
//  Hackathon
//
//  Created by Franz Heinfling on 26.10.13.
//  Copyright (c) 2013 Franz Heinfling. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
