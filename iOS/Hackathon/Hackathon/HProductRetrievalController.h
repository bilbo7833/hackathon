//
//  HProductRetrievalController.h
//  Hackathon
//
//  Created by Franz Heinfling on 26.10.13.
//  Copyright (c) 2013 Franz Heinfling. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HProductRetrievalController : NSObject

+ (void)retrieveFancyProductsAtURL:(NSString*)url WithBlock:(void (^)(NSDictionary *dict, NSError *error))block;

+ (void)retrieveArticleDetailForSKU:(NSString*)sku andDomain:(NSString*)domain withBlock:(void (^) (NSDictionary *dict, NSError *error))block;

@end
