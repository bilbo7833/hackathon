//
//  HViewController.m
//  Hackathon
//
//  Created by Franz Heinfling on 26.10.13.
//  Copyright (c) 2013 Franz Heinfling. All rights reserved.
//

#import "HViewController.h"
#import "HProductRetrievalController.h"
#import "HShopItemModel.h"
#import "HProductRetrievalController.h"
#import "HCollectionViewCell.h"
#import "HOrderViewController.h"

static NSString *kDomain = @"www.zalando.co.uk";
static NSString *kFemaleJackets = @"http://disrupt-hackathon.zalando.net/search/www.zalando.co.uk/womens-clothing-tops";
static NSString *kFemalePants = @"http://disrupt-hackathon.zalando.net/search/www.zalando.co.uk/womens-clothing-trousers";
static NSString *kFemaleShoes = @"http://disrupt-hackathon.zalando.net/search/www.zalando.co.uk/womens-shoes";
int topIndex;
int midIndex;
int botIndex;

@interface HViewController ()
@property (nonatomic, weak) UIImage *topImage;
@property (nonatomic, weak) UIImage *midImage;
@property (nonatomic, weak) UIImage *botImage;
@end

@implementation HViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.

    [self retrieveData];
    [self.orderButton addTarget:self action:@selector(placeAnOrder) forControlEvents:UIControlEventTouchDown];
    [self.navigationItem setTitle:@"ZalandoMatcher"];
    self.navigationController.navigationBar.titleTextAttributes = [NSDictionary dictionaryWithObject:[UIColor colorWithRed:0.941 green:0.502 blue:0.059 alpha:1.0] forKey:NSForegroundColorAttributeName];
}

- (void)placeAnOrder
{
    HShopItemModel *model = [HShopItemModel sharedInstance];
    
    //TOP
    [HProductRetrievalController retrieveArticleDetailForSKU:[model.topSKUs objectAtIndex:topIndex] andDomain:kDomain withBlock:^(NSDictionary *dict, NSError *error) {
        if (error == nil) {
            model.topItems = dict;
        } else {
            //TODO
        }
    }];
    
    //MID
    [HProductRetrievalController retrieveArticleDetailForSKU:[model.middleSKUs objectAtIndex:midIndex] andDomain:kDomain withBlock:^(NSDictionary *dict, NSError *error) {
        if (error == nil) {
            model.midItems = dict;
        } else {
            //TODO
        }
    }];
    
    //BOT
    [HProductRetrievalController retrieveArticleDetailForSKU:[model.bottomSKUs objectAtIndex:topIndex] andDomain:kDomain withBlock:^(NSDictionary *dict, NSError *error) {
        if (error == nil) {
            model.botItems = dict;
            
            UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
            HOrderViewController *ovc = [sb instantiateViewControllerWithIdentifier:@"HOrderViewController"];
            ovc.topImg = _topImage;
            ovc.midImg = _midImage;
            ovc.botImg = _botImage;
            
            [self.navigationController pushViewController:ovc animated:YES];
        } else {
            //TODO
        }
    }];
    
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)retrieveData
{
    HShopItemModel *model = [HShopItemModel sharedInstance];
    [HProductRetrievalController retrieveFancyProductsAtURL:kFemaleJackets WithBlock:^(NSDictionary *dict, NSError *error) {
        if (error == nil) {
            model.topItemsDictionary = dict;
            model.topItemsDictionary = [model.topItemsDictionary objectForKey:@"data"];
            if ([[model.topItemsDictionary valueForKey:@"imageUrl"] isKindOfClass:[NSArray class]]) {
                model.topImages = [NSArray arrayWithArray:[model.topItemsDictionary valueForKey:@"imageUrl"]];
                model.topSKUs = [NSArray arrayWithArray:[model.topItemsDictionary valueForKey:@"sku"]];
                
                [self.topCollectionView reloadData];
            }

        } else {
            // TODO
        }
    }];
    
    [HProductRetrievalController retrieveFancyProductsAtURL:kFemalePants WithBlock:^(NSDictionary *dict, NSError *error) {
        if (error == nil) {
            model.middleItemsDictionary = dict;
            model.middleItemsDictionary = [model.middleItemsDictionary objectForKey:@"data"];
            
            if ([[model.middleItemsDictionary valueForKey:@"imageUrl"] isKindOfClass:[NSArray class]]) {
                model.middleImages = [NSArray arrayWithArray:[model.middleItemsDictionary valueForKey:@"imageUrl"]];
                model.middleSKUs = [NSArray arrayWithArray:[model.middleItemsDictionary valueForKey:@"sku"]];

                [self.middleCollectionView reloadData];
            }
            
        } else {
            // TODO
        }
    }];
    
    [HProductRetrievalController retrieveFancyProductsAtURL:kFemaleShoes WithBlock:^(NSDictionary *dict, NSError *error) {
        if (error == nil) {
            model.bottomItemsDictionary = dict;
            model.bottomItemsDictionary = [model.bottomItemsDictionary objectForKey:@"data"];
            
            if ([[model.bottomItemsDictionary valueForKey:@"imageUrl"] isKindOfClass:[NSArray class]]) {
                model.bottomImages = [NSArray arrayWithArray:[model.bottomItemsDictionary valueForKey:@"imageUrl"]];
                model.bottomSKUs = [NSArray arrayWithArray:[model.bottomItemsDictionary valueForKey:@"sku"]];

                [self.bottomCollectionView reloadData];
            }
        } else {
            // TODO
        }
    }];
}


#pragma mark - UICollectionViewDataSource -
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    HShopItemModel *model = [HShopItemModel sharedInstance];

    if (collectionView == self.topCollectionView) {
        return model.topImages.count;
    } else if (collectionView == self.middleCollectionView) {
        return model.middleImages.count;
    } else if (collectionView == self.bottomCollectionView) {
        return model.bottomImages.count;
    }
    
    return 0;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    HShopItemModel *model = [HShopItemModel sharedInstance];

    if (collectionView == self.topCollectionView) {
        HCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"topCell" forIndexPath:indexPath];
        _topImage = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[model.topImages objectAtIndex:indexPath.row]]]];
        [cell.imageView setImage:_topImage];
        cell.imageView.contentMode = UIViewContentModeScaleAspectFit;
        topIndex = indexPath.row;
        
        return  cell;
        
    } else if (collectionView == self.middleCollectionView) {
        HCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"midCell" forIndexPath:indexPath];
        cell.imageView.contentMode = UIViewContentModeScaleAspectFit;
        _midImage = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[model.middleImages objectAtIndex:indexPath.row]]]];
        [cell.imageView setImage:_midImage];
        midIndex = indexPath.row;

        return cell;
        
    } else if (collectionView == self.bottomCollectionView) {
        HCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"botCell" forIndexPath:indexPath];
        [cell.imageViewShoe setContentMode:UIViewContentModeScaleAspectFit];
        _botImage = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[model.bottomImages objectAtIndex:indexPath.row]]]];
        [cell.imageViewShoe setImage:_botImage];
        botIndex = indexPath.row;
        
        return cell;
    }

    return nil;
}

@end
