//
//  HShopItemModel.m
//  Hackathon
//
//  Created by Franz Heinfling on 26.10.13.
//  Copyright (c) 2013 Franz Heinfling. All rights reserved.
//

#import "HShopItemModel.h"
#import "HProductRetrievalController.h"
#import "HShopItemModel.h"

@implementation HShopItemModel

+ (id)sharedInstance
{
    static HShopItemModel *shopItemModel;
    
    if (!shopItemModel) {
        shopItemModel = [[HShopItemModel alloc] init];
    }
    
    return shopItemModel;
}

@end
