//
//  HCollectionViewCell.h
//  Hackathon
//
//  Created by Franz Heinfling on 26.10.13.
//  Copyright (c) 2013 Franz Heinfling. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HCollectionViewCell : UICollectionViewCell
@property (nonatomic, weak) IBOutlet UIImageView *imageView;
@property (nonatomic, weak) IBOutlet UIImageView *imageViewShoe;
@end
