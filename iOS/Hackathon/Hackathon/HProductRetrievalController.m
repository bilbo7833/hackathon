//
//  HProductRetrievalController.m
//  Hackathon
//
//  Created by Franz Heinfling on 26.10.13.
//  Copyright (c) 2013 Franz Heinfling. All rights reserved.
//

#import "HProductRetrievalController.h"
#import <AFNetworking/AFNetworking.h>
#import "HShopItemModel.h"

@implementation HProductRetrievalController

+ (void)retrieveFancyProductsAtURL:(NSString*)url WithBlock:(void (^)(NSDictionary *dict, NSError *error))block
{
    NSMutableURLRequest *urlReq = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:url]];
    [urlReq setValue:@"application/json" forHTTPHeaderField:@"Accept"];

    AFHTTPRequestOperation *reqOp = [[AFHTTPRequestOperation alloc] initWithRequest:urlReq];
    reqOp.responseSerializer = [AFJSONResponseSerializer serializer];
    
    [reqOp setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        block([responseObject objectForKeyedSubscript:@"searchResults"], nil);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        block(nil, error);
    }];

    [[NSOperationQueue mainQueue] addOperation:reqOp];
}

+ (void)retrieveArticleDetailForSKU:(NSString*)sku andDomain:(NSString*)domain withBlock:(void (^) (NSDictionary *dict, NSError *error))block
{
    NSMutableURLRequest *urlReq = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/%@/%@", @"http://disrupt-hackathon.zalando.net/article", domain, sku]]];
    [urlReq setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    AFHTTPRequestOperation *reqOp = [[AFHTTPRequestOperation alloc] initWithRequest:urlReq];
    reqOp.responseSerializer = [AFJSONResponseSerializer serializer];
    
    [reqOp setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSArray *size = [[[responseObject objectForKey:@"simples"] objectForKey:@"data"] valueForKey:@"size"];
        NSArray *basketURL = [[[responseObject objectForKey:@"simples"] objectForKey:@"data"] valueForKey:@"addToBasketUrl"];
        block([NSDictionary dictionaryWithObjects:basketURL forKeys:size], nil);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        block(nil, error);
    }];

    [[NSOperationQueue mainQueue] addOperation:reqOp];
}

@end
