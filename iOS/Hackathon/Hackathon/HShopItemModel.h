//
//  HShopItemModel.h
//  Hackathon
//
//  Created by Franz Heinfling on 26.10.13.
//  Copyright (c) 2013 Franz Heinfling. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HShopItemModel : NSObject

@property (nonatomic, strong) NSDictionary *topItemsDictionary;
@property (nonatomic, strong) NSArray *topImages;
@property (nonatomic, strong) NSArray *topSKUs;

@property (nonatomic, strong) NSDictionary *middleItemsDictionary;
@property (nonatomic, strong) NSArray *middleImages;
@property (nonatomic, strong) NSArray *middleSKUs;

@property (nonatomic, strong) NSDictionary *bottomItemsDictionary;
@property (nonatomic, strong) NSArray *bottomImages;
@property (nonatomic, strong) NSArray *bottomSKUs;


//Order related
@property (nonatomic, strong) NSDictionary *topItems;
@property (nonatomic, strong) NSDictionary *midItems;
@property (nonatomic, strong) NSDictionary *botItems;


+ (id)sharedInstance;

@end
