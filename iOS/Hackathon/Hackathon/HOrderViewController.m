//
//  HOrderViewController.m
//  Hackathon
//
//  Created by Franz Heinfling on 27.10.13.
//  Copyright (c) 2013 Franz Heinfling. All rights reserved.
//

#import "HOrderViewController.h"
#import "HShopItemModel.h"
#import <AFNetworking/AFNetworking.h>

int topIndex = 0;
int midIndex = 0;
int botIndex = 0;

@interface HOrderViewController ()

@end

@implementation HOrderViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    _topPicker.delegate = self;
    _topPicker.dataSource = self;
    _midPicker.delegate = self;
    _midPicker.dataSource = self;
    _botPicker.delegate = self;
    _botPicker.dataSource = self;
    
    _topImageView.image = _topImg;
    _topImageView.contentMode = UIViewContentModeScaleAspectFit;
    _midImageVIew.image = _midImg;
    _midImageVIew.contentMode = UIViewContentModeScaleAspectFit;
    _botImageView.image = _botImg;
    _botImageView.contentMode = UIViewContentModeScaleAspectFit;
    
    [_orderButton addTarget:self action:@selector(sendOrderAndOpenWeb) forControlEvents:UIControlEventTouchDown];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)sendOrderAndOpenWeb
{
    HShopItemModel *model = [HShopItemModel sharedInstance];
    
    NSLog(@"top %@", [model.topItems objectForKey:[[model.topItems keysSortedByValueUsingSelector:@selector(compare:)] objectAtIndex:topIndex]]);
    
    NSLog(@"mid %@", [model.midItems objectForKey:[[model.midItems keysSortedByValueUsingSelector:@selector(compare:)] objectAtIndex:midIndex]]);
    
    NSLog(@"bot %@", [model.botItems objectForKey:[[model.botItems keysSortedByValueUsingSelector:@selector(compare:)] objectAtIndex:botIndex]]);

    NSMutableURLRequest *urlReq = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:[model.topItems objectForKey:[[model.topItems keysSortedByValueUsingSelector:@selector(compare:)] objectAtIndex:topIndex]]]];
    
    AFHTTPRequestOperation *reqOp = [[AFHTTPRequestOperation alloc] initWithRequest:urlReq];
    
    [reqOp setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {

    }];
    
    [[NSOperationQueue mainQueue] addOperation:reqOp];

    
    //MID
    urlReq = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:[model.midItems objectForKey:[[model.midItems keysSortedByValueUsingSelector:@selector(compare:)] objectAtIndex:midIndex]]]];
    
    reqOp = [[AFHTTPRequestOperation alloc] initWithRequest:urlReq];
    
    [reqOp setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
    }];
    
    [[NSOperationQueue mainQueue] addOperation:reqOp];
    
    //BOT
    urlReq = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:[model.botItems objectForKey:[[model.botItems keysSortedByValueUsingSelector:@selector(compare:)] objectAtIndex:botIndex]]]];
    
    reqOp = [[AFHTTPRequestOperation alloc] initWithRequest:urlReq];
    
    [reqOp setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {

        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
    }];
    
    NSMutableURLRequest *urlReq2 = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:[model.midItems objectForKey:[[model.midItems keysSortedByValueUsingSelector:@selector(compare:)] objectAtIndex:midIndex]]]];

    NSMutableURLRequest *urlReq3 = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:[model.topItems objectForKey:[[model.topItems keysSortedByValueUsingSelector:@selector(compare:)] objectAtIndex:topIndex]]]];

    UIWebView *webv = [[UIWebView alloc] initWithFrame:self.view.frame];
    [self.view addSubview:webv];
    [webv loadRequest:urlReq];
    [webv loadRequest:urlReq2];
    [webv loadRequest:urlReq3];
    
    [[NSOperationQueue mainQueue] addOperation:reqOp];

}

#pragma mark - PickerDataSource -

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    HShopItemModel *model = [HShopItemModel sharedInstance];

    if (pickerView == self.topPicker) {
        return model.topItems.count;
    } else if (pickerView == self.midPicker) {
        return model.midItems.count;
    } else if (pickerView == self.botPicker) {
        return model.botItems.count;
    }
    
    return 0;
}

#pragma mark - PickerDelegate -

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    HShopItemModel *model = [HShopItemModel sharedInstance];

    if (pickerView == self.topPicker) {
        return [[model.topItems keysSortedByValueUsingSelector:@selector(compare:)] objectAtIndex:row];
    } else if (pickerView == self.midPicker) {
        return [[model.midItems keysSortedByValueUsingSelector:@selector(compare:)] objectAtIndex:row];
    } else if (pickerView == self.botPicker) {
        return [[model.botItems keysSortedByValueUsingSelector:@selector(compare:)] objectAtIndex:row];
    }
    
    return nil;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    if (pickerView == _topPicker) {
        topIndex = row;
    } else if (pickerView == _midPicker) {
        midIndex = row;
    } else if (pickerView == _botPicker) {
        botIndex = row;
    }
}
@end
