package com.projecta.hackathon.zalando;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.app.Application;

import com.projecta.hackathon.zalando.http.WebkitCookieManagerProxy;
import com.projecta.hackathon.zalando.models.Article;

public class ZalandoMatcherApplication extends Application
{

    Map< String, List< Article > > cfArticleTops  = new HashMap< String, List< Article > >();
    Map< String, List< Article > > cfArticlePants = new HashMap< String, List< Article > >();
    Map< String, List< Article > > cfArticleShoes = new HashMap< String, List< Article > >();

    Map< String, String[] >        matches        = new HashMap< String, String[] >();

    @Override
    public void onCreate()
    {
        super.onCreate();

        android.webkit.CookieSyncManager.createInstance( this );
        // unrelated, just make sure cookies are generally allowed
        android.webkit.CookieManager.getInstance().setAcceptCookie( true );

        // magic starts here
        WebkitCookieManagerProxy coreCookieManager =
            new WebkitCookieManagerProxy( null, java.net.CookiePolicy.ACCEPT_ALL );
        java.net.CookieHandler.setDefault( coreCookieManager );
        //matches.put( "Blue", new String[] {"White","Black","Gray", "Brown"} );
        //matches.put( "Green", new String[] {"White","Black", "Gray","Red"} );
        //matches.put( "White", new String[] {"Blue","Black","Green","Gray"} );
        //matches.put( "Black", new String[] {"Blue","Black","Green","Gray","Red"} );
        //matches.put( "Red" , new String[] {"White", "Black", "Gray", "Brown"});
        matches.put( "Blue", new String[] {"White"} );
        matches.put( "White", new String[] {"Blue"} );
        matches.put( "Green", new String[] {"Black"} );
        matches.put( "Black", new String[] {"Green"} );
        matches.put( "Red", new String[] {"Gray"} );
        matches.put( "Gray", new String[] {"Red"} );
    }

    public Map< String, List< Article >> getCfTops()
    {
        return cfArticleTops;
    }

    public Map< String, List< Article >> getCfPants()
    {
        return cfArticlePants;
    }

    public Map< String, List< Article >> getCfShoes()
    {
        return cfArticleShoes;
    }

    public Map< String, String[] > getMatches()
    {
        return matches;
    }

}
