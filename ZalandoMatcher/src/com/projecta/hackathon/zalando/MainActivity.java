package com.projecta.hackathon.zalando;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import android.animation.Animator;
import android.app.LoaderManager;
import android.content.Intent;
import android.content.Loader;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.projecta.hackathon.zalando.adapter.ArticlePagerAdapter;
import com.projecta.hackathon.zalando.fragment.ImageFragment.OnImageClick;
import com.projecta.hackathon.zalando.fragment.ImageFragment.OnLongImageClick;
import com.projecta.hackathon.zalando.http.BuyItem;
import com.projecta.hackathon.zalando.http.BuyItem.BuyCallback;
import com.projecta.hackathon.zalando.http.HttpClientNew;
import com.projecta.hackathon.zalando.loader.ZalandoApiLoader;
import com.projecta.hackathon.zalando.models.Article;
import com.projecta.hackathon.zalando.widgets.CustomViewPager;

public class MainActivity extends FragmentActivity implements OnImageClick, OnLongImageClick,
        LoaderManager.LoaderCallbacks< List< Object > >, BuyCallback
{

    private static final String TAG                     = "MainActivity";

    private CustomViewPager     mTopsPager, mPantsPager, mShoesPager;

    private Animator            mCurrentAnimator;

    private ArticlePagerAdapter mPantsAdapter;
    private ArticlePagerAdapter mShoesAdapter;
    private ArticlePagerAdapter mTopsAdapter;

    public final static int     TOP                     = 1;
    public final static int     MIDDLE                  = 2;
    public final static int     BOTTOM                  = 3;

    private final static int    ARTICLE_DETAILS_TOP     = 10;
    private final static int    ARTICLE_DETAILS_MIDDLE  = 20;
    private final static int    ARTICLE_DETAILS_BOTTOM  = 30;

    // private List< Article > order;
    private Article             mTopArticle;
    private Article             mMiddleArticle;
    private Article             mBottomArticle;

    int                         articleDetailsRetrieved = -1;
    int                         articlesOrdered         = -1;
    boolean                     loadersStarted          = false;

    String topUrl =
            "http://disrupt-hackathon.zalando.net/search/www.zalando.co.uk/womens-clothing-tops";
    String middleUrl =
            "http://disrupt-hackathon.zalando.net/search/www.zalando.co.uk/womens-clothing-trousers";
    String bottomUrl =
            "http://disrupt-hackathon.zalando.net/search/www.zalando.co.uk/womens-shoes";


    private HttpClientNew       mClient;

    private final static String DETAILS_URL             =
                                                            "http://disrupt-hackathon.zalando.net/article/www.zalando.co.uk/";

    @Override
    protected void onCreate( Bundle savedInstanceState )
    {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_main );

        // Init stuff---
        mTopsAdapter = new ArticlePagerAdapter( getSupportFragmentManager(), TOP, null );
        mPantsAdapter = new ArticlePagerAdapter( getSupportFragmentManager(), MIDDLE, null );
        mShoesAdapter = new ArticlePagerAdapter( getSupportFragmentManager(), BOTTOM, null );

        mPantsPager = ( CustomViewPager ) findViewById( R.id.pager_pants );
        mTopsPager = ( CustomViewPager ) findViewById( R.id.pager_tops );
        mShoesPager = ( CustomViewPager ) findViewById( R.id.pager_shoes );

        String topUrl =
            "http://disrupt-hackathon.zalando.net/search/www.zalando.co.uk/womens-clothing-tops";
        String middleUrl =
            "http://disrupt-hackathon.zalando.net/search/www.zalando.co.uk/womens-clothing-trousers";
        String bottomUrl =
            "http://disrupt-hackathon.zalando.net/search/www.zalando.co.uk/womens-shoes";

        Bundle topBundle = new Bundle();
        topBundle.putString( "url", topUrl );

        Bundle middleBundle = new Bundle();
        middleBundle.putString( "url", middleUrl );

        Bundle bottomBundle = new Bundle();
        bottomBundle.putString( "url", bottomUrl );

        getLoaderManager().initLoader( TOP, topBundle, this );
        getLoaderManager().initLoader( MIDDLE, middleBundle, this );
        getLoaderManager().initLoader( BOTTOM, bottomBundle, this );

        // Binding----
        mShoesPager.setAdapter( mShoesAdapter );
        mTopsPager.setAdapter( mTopsAdapter );
        mPantsPager.setAdapter( mPantsAdapter );

    }

    @Override
    public boolean onCreateOptionsMenu( Menu menu )
    {
        // Inflate the menu items for use in the action bar
        MenuInflater inflater = getMenuInflater();
        inflater.inflate( R.menu.main, menu );
        return super.onCreateOptionsMenu( menu );
    }

    @Override
    public boolean onOptionsItemSelected( MenuItem item )
    {
        // Handle presses on the action bar items
        switch ( item.getItemId() )
        {
            case R.id.action_buy:
                buyItems();
                return true;
            default:
                return super.onOptionsItemSelected( item );
        }
    }

    private void buyItems()
    {
        int topItemPos = mTopsPager.getCurrentItem();
        mTopArticle = ( Article ) mTopsAdapter.getArticle( topItemPos );

        int middleItemPos = mPantsPager.getCurrentItem();
        mMiddleArticle = ( Article ) mPantsAdapter.getArticle( middleItemPos );

        int bottomItemPos = mShoesPager.getCurrentItem();
        mBottomArticle = ( Article ) mShoesAdapter.getArticle( bottomItemPos );

        Log.d( TAG, "Trying to buy items: " + mTopArticle + mMiddleArticle + mBottomArticle );

        articleDetailsRetrieved = 0;

        // Starting loaders for the details
        Bundle topBundle = new Bundle();
        topBundle.putString( "url", DETAILS_URL + mTopArticle.getSku() );
        topBundle.putInt( "id", TOP );
        if ( loadersStarted )
        {
            getLoaderManager().restartLoader( ARTICLE_DETAILS_TOP, topBundle, this );
        }
        else
        {
            getLoaderManager().initLoader( ARTICLE_DETAILS_TOP, topBundle, this );
        }

        Bundle middleBundle = new Bundle();
        middleBundle.putString( "url", DETAILS_URL + mMiddleArticle.getSku() );
        middleBundle.putInt( "id", MIDDLE );
        if ( loadersStarted )
        {
            getLoaderManager().restartLoader( ARTICLE_DETAILS_MIDDLE, middleBundle, this );
        }
        else
        {
            getLoaderManager().initLoader( ARTICLE_DETAILS_MIDDLE, middleBundle, this );
        }

        Bundle bottomBundle = new Bundle();
        bottomBundle.putString( "url", DETAILS_URL + mBottomArticle.getSku() );
        bottomBundle.putInt( "id", BOTTOM );
        if ( loadersStarted )
        {
            getLoaderManager().restartLoader( ARTICLE_DETAILS_BOTTOM, bottomBundle, this );
        }
        else
        {
            getLoaderManager().initLoader( ARTICLE_DETAILS_BOTTOM, bottomBundle, this );
        }

        loadersStarted = true;
    }

    @Override
    public Loader< List< Object >> onCreateLoader( int id, Bundle args )
    {
        Log.i( TAG, "onCreateLoader() called!" );

        return new ZalandoApiLoader( this, args.getString( "url" ) );

    }

    @Override
    public void onLoadFinished( Loader< List< Object >> loader, List< Object > results )
    {
        Log.i( TAG, "onLoadFinished() called!" );
        switch ( loader.getId() )
        {
            case TOP:
            {
                mTopsAdapter.setData( results );
                if ( results != null )
                {
                    mTopArticle = ( Article ) results.get( 0 );
                }
                break;
            }
            case MIDDLE:
            {
                mPantsAdapter.setData( results );
                if ( results != null )
                {
                    mMiddleArticle = ( Article ) results.get( 0 );
                }
                break;
            }
            case BOTTOM:
            {
                mShoesAdapter.setData( results );
                if ( results != null )
                {
                    mBottomArticle = ( Article ) results.get( 0 );
                }
                break;
            }
            case ARTICLE_DETAILS_TOP:
            {
                mTopArticle.setDetailsObjects( results );
                loader.reset();
                articleDetailsRetrieved++;
                break;
            }
            case ARTICLE_DETAILS_MIDDLE:
            {
                mMiddleArticle.setDetailsObjects( results );
                loader.reset();
                articleDetailsRetrieved++;
                break;
            }
            case ARTICLE_DETAILS_BOTTOM:
            {
                mBottomArticle.setDetailsObjects( results );
                loader.reset();
                articleDetailsRetrieved++;
                break;
            }
        }
        if ( articleDetailsRetrieved == 3 )
        {
            showCheckout();
        }

    }

    private void showCheckout()
    {
        Log.d( TAG, "Ready for next activity" );

        mClient = new HttpClientNew();
        BuyItem buy = new BuyItem( this, mClient, this, null );

        articlesOrdered = 0;

        buy.execute( mTopArticle.getDetails().get( 0 ).getAddToBasketUrl() );
        // put the 3 times in the bundle and
        // Goto next Activity

    }

    @Override
    public void onLoaderReset( Loader< List< Object >> loader )
    {
        Log.i( TAG, "onLoadReset() called!" );
        // loader.cancelLoad();
    }

    @Override
    public void onImageClicked( Article article, int parentId )
    {
        Log.d( TAG, "SKU" + article.getSku() );
        /*
         * final FragmentManager fm = getSupportFragmentManager(); final
         * FragmentTransaction ft = fm.beginTransaction(); ft.replace(
         * android.R.id.content, ImageFragment.newInstance( article, 0 ) );
         * ft.addToBackStack( "detailview" ); ft.commit();
         */
    }

    @Override
    public void onLongImageClick( int id )
    {
        Log.d( TAG, "PAGER ID " + id );
        switch ( id )
        {
            case TOP:
                tooglePager( mTopsPager, mTopArticle );
                // Log.d( TAG, "Is enabled: " + mTopsPager.isEnabled() );
                return;

            case MIDDLE:
                tooglePager( mPantsPager, mMiddleArticle );
                return;
            case BOTTOM:
                tooglePager( mShoesPager, mBottomArticle );
                return;
            default:
                break;
        }

        Log.d( TAG, "Long click " );
    }

    private void tooglePager( final CustomViewPager pager, final Article article )
    {
        Log.d( TAG, "Status pager: " + pager.isPagingEnabled() );
        if ( pager.isPagingEnabled() )
        {
            pager.setPagingEnabled( false );
            switch ( pager.getId() )
            {
                case R.id.pager_tops:
                    setDataForPager(
                            mPantsAdapter, mTopArticle,
                            ( ( ZalandoMatcherApplication ) getApplication() ).getCfPants() );
                    setDataForPager(
                            mShoesAdapter, mTopArticle,
                            ( ( ZalandoMatcherApplication ) getApplication() ).getCfShoes() );
                    break;

                case R.id.pager_pants:
                    setDataForPager(
                            mTopsAdapter, mMiddleArticle,
                            ( ( ZalandoMatcherApplication ) getApplication() ).getCfTops() );
                    setDataForPager(
                            mShoesAdapter, mMiddleArticle,
                            ( ( ZalandoMatcherApplication ) getApplication() ).getCfShoes() );
                    break;
                case R.id.pager_shoes:
                    setDataForPager(
                            mPantsAdapter, mBottomArticle,
                            ( ( ZalandoMatcherApplication ) getApplication() ).getCfPants() );
                    setDataForPager(
                            mTopsAdapter, mBottomArticle,
                            ( ( ZalandoMatcherApplication ) getApplication() ).getCfTops() );
                default:
                    break;
            }
        }
        else
        {

            Bundle topBundle = new Bundle();
            topBundle.putString( "url", topUrl );

            Bundle middleBundle = new Bundle();
            middleBundle.putString( "url", middleUrl );

            Bundle bottomBundle = new Bundle();
            bottomBundle.putString( "url", bottomUrl );

            getLoaderManager().restartLoader( TOP, topBundle, this );
            getLoaderManager().restartLoader( MIDDLE, middleBundle, this );
            getLoaderManager().restartLoader( BOTTOM, bottomBundle, this );
            pager.setPagingEnabled( true );
        }
    }

    @Override
    public void itemCalled( List< String > cookie )
    {
        articlesOrdered++;
        Log.d( TAG, "Articles ordered: " + articlesOrdered );
        if ( articlesOrdered == 2 )
        {
            Intent i = new Intent( getApplicationContext(), WebviewActivity.class );
            // Intent i = new Intent(Intent.ACTION_VIEW,
            // Uri.parse(mBottomArticle.getDetails().get( 0
            // ).getAddToBasketUrl() ));
            Bundle bundle = new Bundle();
//            ArrayList< String > cookieHeaders = new ArrayList< String >();
//            for ( String cook: cookie )
//            {
//                cookieHeaders.add( cook );
//            }
//            bundle.putStringArrayList( "Cookie", cookieHeaders );
            bundle.putString( "url", mBottomArticle.getDetails().get( 0 ).getAddToBasketUrl() );
            i.putExtras( bundle );
            // if ( cookie != null )
            // {
            // Log.d( TAG, "Starting intent with Cookie: " + cookie );
            // String cookStr = "";
            // for ( int j =0; j< cookie.size(); j++ )
            // {
            // cookStr = cookStr + cookie.get( j );
            // if( j < cookie.size() - 1 )
            // {
            // cookStr += ", ";
            // }
            // }
            // Log.d( TAG, "CookieStr: " + cookStr );
            // bundle.putString( "Cookie", cookStr );
            // }
            // i.putExtra( Browser.EXTRA_HEADERS, bundle );
            startActivity( i );
        }
        else
        {
            Log.d( TAG, "Cookie: " + cookie );
            if ( cookie != null )
            {
                BuyItem nextBuy = new BuyItem( this, mClient, this, cookie );
                nextBuy.execute( mMiddleArticle.getDetails().get( 0 ).getAddToBasketUrl() );
            }
        }
    }

    private void setDataForPager( ArticlePagerAdapter adapter, Article article,
            Map< String, List< Article >> map )
    {
        final String key = article.getColorFamily();
        String[] matches =
            ( ( ZalandoMatcherApplication ) getApplication() ).getMatches().get( key );

        Log.d( TAG, "key" + key );
        ArrayList< Object > items = new ArrayList< Object >();
        for ( String match: matches )
        {
            Log.d( TAG, "Match " + match );
            if ( map.containsKey( match ) )
            {
                items.addAll( map.get( match ) );
            }
        }
        adapter.setData( items );
    }
}
