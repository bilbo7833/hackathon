package com.projecta.hackathon.zalando.http;

/*
 * Copyright 2011 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import java.util.List;
import java.util.Map;

import android.net.Uri;

/**
 * Represents the result of an HttpClient call.
 */
public class Response {

    /**
     * The HTTP status code
     */
    public int                       status;

    /**
     * The HTTP status message
     */
    public String                    message;

    /**
     * The HTTP headers received in the response
     */
    public Map<String, List<String>> headers;

    /**
     * The response body, if any
     */
    public byte[]                    body;

    public Uri                       resourcePath = null;

    protected Response(int status, String message, Map<String, List<String>> headers, byte[] body, Uri resourcePath) {
        this.status = status;
        this.message = message;
        this.headers = headers;
        this.body = body;
        this.resourcePath =resourcePath;
    }
}