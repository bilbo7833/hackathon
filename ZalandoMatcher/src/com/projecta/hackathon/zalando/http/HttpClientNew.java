package com.projecta.hackathon.zalando.http;

/*
 * Copyright 2011 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import java.io.BufferedInputStream;
import java.io.File;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.Proxy;
import java.net.URL;
import java.util.List;
import java.util.Map;

import android.net.Uri;
import android.os.Environment;
import android.util.Log;

import com.projecta.hackathon.zalando.utils.IOUtils;


/**
 * Performs GET and POST operations against a Google App Engine app,
 * authenticating with a Google account on the device.
 */
public class HttpClientNew {

    private static String TAG          = "HttpClient";

    private String        mErrorMessage;

    private boolean       authenticate = false;

    /**
     * Constructs a client, which may be used for multiple requests. This
     * constructor may be called on the UI thread.
     *
     * @param appURI
     *            The URI of the App Engine app you want to interact with, e.g.
     *            your-app.appspot.com
     * @param account
     *            The android.accounts.Account representing the Google account
     *            to authenticate with.
     * @param context
     *            Used, if necessary, to prompt user for authentication
     */
    public HttpClientNew() {
        System.setProperty( "http.keepAlive", "false" );
    }

    /**
     * Constructs a client, which may be used for multiple requests, and which
     * will never prompt the user for authentication input. This constructor may
     * be called on the UI thread but is suitable for use on background threads
     * with no access to an Activity.
     *
     * @param appURI
     *            The URI of the App Engine app you want to interact with, e.g.
     *            your-app.appspot.com
     * @param authToken
     *            AccountManager authtoken, e.g. from the token() method
     * @param context
     *            Used to look up strings
     */
    public HttpClientNew(String authToken) {
    }

    /**
     * Performs an HTTP GET request. The request is performed inline and this
     * method must not be called from the UI thread.
     *
     * @param uri
     *            The URI you're sending the GET to
     * @param headers
     *            Any extra HTTP headers you want to send; may be null
     * @return a Response structure containing the status, headers, and body.
     *         Returns null if the request could not be launched due to an IO
     *         error or authentication failure; in which case use errorMessage()
     *         to retrieve diagnostic information.
     */
    public Response get( URL uri, Map<String, List<String>> headers, Uri responsePayload ) {
        GET get = new GET( uri, headers, responsePayload );
        return sendRequest( get );
    }

    /**
     * Performs an HTTP GET request. The request is performed inline and this
     * method must not be called from the UI thread.
     *
     * @param uri
     *            The URI you're sending the GET to
     * @param headers
     *            Any extra HTTP headers you want to send; may be null
     * @return a Response structure containing the status, headers, and body.
     *         Returns null if the request could not be launched due to an IO
     *         error or authentication failure; in which case use errorMessage()
     *         to retrieve diagnostic information.
     */
    public Response get( URL uri, Map<String, List<String>> headers ) {
        GET get = new GET( uri, headers, null );
        return sendRequest( get );
    }

    /**
     * Performs an HTTP POST request. The request is performed inline and this
     * method must not be called from the UI thread.
     *
     * @param uri
     *            The URI you're sending the POST to
     * @param headers
     *            Any extra HTTP headers you want to send; may be null
     * @param body
     *            The request body to transmit
     * @return a Response structure containing the status, headers, and body.
     *         Returns null if the request could not be launched due to an IO
     *         error or authentication failure; in which case use errorMessage()
     *         to retrieve diagnostic information.
     */
    public Response post( URL uri, Map<String, List<String>> headers, byte[] body,
            Uri responsePayload ) {
        POST post = new POST( uri, headers, body, responsePayload );
        return sendRequest( post );
    }

    /**
     * Performs an HTTP DELETE request. The request is performed inline and this
     * method must not be called from the UI thread.
     *
     * @param uri
     *            The URI you're sending the DELETE to
     * @param headers
     *            Any extra HTTP headers you want to send; may be null
     * @param body
     *            The request body to transmit
     * @return a Response structure containing the status, headers, and body.
     *         Returns null if the request could not be launched due to an IO
     *         error or authentication failure; in which case use errorMessage()
     *         to retrieve diagnostic information.
     */
    public Response delete( URL uri, Map<String, List<String>> headers, byte[] body, Uri responsePayload ) {
        DELETE delete = new DELETE( uri, headers, body, responsePayload );
        return sendRequest( delete );
    }

    /**
     * Provides an error message should a get() or post() return null.
     *
     * @return the message
     */
    public String errorMessage() {
        return mErrorMessage;
    }

    private Response sendRequest( Request request ) {
        Log.d( TAG, "Trying HTTP Request to " + request.uri );
        mErrorMessage = null;
        HttpURLConnection conn = null;

        Response response = null;
        try {
            conn = ( HttpURLConnection ) request.uri.openConnection( Proxy.NO_PROXY);
            if ( authenticate ) {
                // authenticate
            } else {
                if ( request.headers != null ) {
                    for ( String header : request.headers.keySet() ) {
                        for ( String value : request.headers.get( header ) ) {
                            conn.addRequestProperty( header, value );
                        }
                    }
                }
                if ( request instanceof POST ) {
                    byte[] payload = ( ( POST ) request ).body;
                    conn.setDoOutput( true );
                    conn.setFixedLengthStreamingMode( payload.length );
                    conn.getOutputStream().write( payload );
                    int status = conn.getResponseCode();
                    String message = conn.getResponseMessage();
                    Log.d( TAG, "Response " + status + " - " + message );
                    if ( status / 100 != 2 ) {
                        BufferedInputStream in = new BufferedInputStream( conn.getErrorStream() );
                        byte[] body = IOUtils.bytesFromStream( in );
                        response = new Response( status, message, conn.getHeaderFields(), body, null );
                    }
                }
                if ( request instanceof DELETE ) {
                    conn.setRequestMethod("DELETE");
                    if (( ( DELETE ) request ).body != null) {
                        byte[] payload = ( ( DELETE ) request ).body;
                        conn.setDoOutput( true );
                        conn.setFixedLengthStreamingMode( payload.length );
                        conn.getOutputStream().write( payload );
                        int status = conn.getResponseCode();
                        String message = conn.getResponseMessage();
                        Log.d( TAG, "Response " + status + " - " + message );
                        if ( status / 100 != 2 ) {
                            BufferedInputStream in = new BufferedInputStream( conn.getErrorStream() );
                            byte[] body = IOUtils.bytesFromStream( in );
                            response = new Response( status, message, conn.getHeaderFields(), body, null );
                        }
                    }
                }
                if ( response == null ) {
                    BufferedInputStream in = new BufferedInputStream( conn.getInputStream() );
                    if ( request.responsePayload != null ) {

                        String externalState = Environment.getExternalStorageState();
                        if ( !Environment.MEDIA_MOUNTED.equals( externalState ) ) {
                            Log.e( TAG, "External Media NOT mounted." );
                        } else {
                            File file = new File( request.responsePayload.getPath() );
                            try {
                                IOUtils.streamToFile( in, file );
                                response = new Response( conn.getResponseCode(),
                                        conn.getResponseMessage(), conn.getHeaderFields(), null,
                                        Uri.fromFile( file ) );
                            } catch ( IOException e ) {
                                Log.e( TAG, "Writing stream to file failed.", e );
                            }
                        }
                    }
                    if ( response == null ) {
                        byte[] body = IOUtils.bytesFromStream( in );
                        response = new Response( conn.getResponseCode(), conn.getResponseMessage(),
                                conn.getHeaderFields(), body, null );
                    }
                }
            }
        } catch ( IOException e ) {
            e.printStackTrace( System.err );
            mErrorMessage = ( ( request instanceof POST ) ? "POST " : "GET " ) + "failed: "
                    + e.getLocalizedMessage();
        } finally {
            if ( conn != null )
                conn.disconnect();
        }
        return response;
    }

    // request structs
    private class Request {
        public URL                       uri;
        public Map<String, List<String>> headers;
        public Uri                       responsePayload = null;

        public Request(URL uri, Map<String, List<String>> headers, Uri responsePayload) {
            this.uri = uri;
            this.headers = headers;
            this.responsePayload = responsePayload;
        }
    }

    private class POST extends Request {
        public byte[] body;

        public POST(URL uri, Map<String, List<String>> headers, byte[] body, Uri responsePayload) {
            super( uri, headers, responsePayload );
            this.body = body;
        }
    }

    private class GET extends Request {

        public GET(URL uri, Map<String, List<String>> headers, Uri responsePayload) {
            super( uri, headers, responsePayload );

        }
    }

    private class DELETE extends Request {
        public byte[] body;

        public DELETE(URL uri, Map<String, List<String>> headers, byte[] body, Uri responsePayload) {
            super( uri, headers, responsePayload );
            this.body = body;
        }
    }

}
