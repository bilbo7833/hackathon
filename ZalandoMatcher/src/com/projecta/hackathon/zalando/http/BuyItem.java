package com.projecta.hackathon.zalando.http;

import java.lang.reflect.Type;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.Charset;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import android.content.Context;
import android.os.AsyncTask;
import android.provider.SyncStateContract.Constants;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.projecta.hackathon.zalando.utils.Beans;
import com.projecta.hackathon.zalando.utils.CountryUtils;
import com.projecta.hackathon.zalando.utils.Utils;

public class BuyItem extends AsyncTask< String, String, List<String> >
{

    public interface BuyCallback
    {
        public void itemCalled( List< String > cookie );
    }

    Context mCtx;

    private static String TAG = "BuyItem";
    private List<String> mCookie = null;
    private BuyCallback mCallback;
    private HttpClientNew mClient;

    public BuyItem( Context context, HttpClientNew client,BuyCallback call, List<String> cookie )
    {
        mCtx = context;
        mCookie = cookie;
        mCallback = call;
        mClient = client;
    }

    @Override
    protected List<String> doInBackground( String... params )
    {
        Log.d( TAG, "Starting Encoding Job." );
        String addToBasketUrl = params[ 0 ];
//        mFilename = params[ 0 ];


        URL buyUrl;
        try
        {
            buyUrl = new URL( addToBasketUrl );
        }
        catch ( MalformedURLException e )
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return null;
        }

        Map<String, List<String>> headers = new HashMap< String, List< String > >();
//        headers.put( "Accept", Beans.toList( "application/json" ) );
//        if (mCookie != null )
//        {
//            headers.put( "Cookie", mCookie );
//        }

        Response response = mClient.get( buyUrl, headers );
        if ( response != null )
        {

            String body = new String(response.body);
//            Log.d( TAG, "Got response: " + body );
            Log.v( TAG, "POST response: " + response.status + " - " + response.message );


            Map< String, List< String >> responseHeaders = response.headers;
            Log.d( TAG, "Got headers: " + responseHeaders );
            if ( responseHeaders.containsKey( "Set-Cookie" ))
            {
                return responseHeaders.get( "Set-Cookie" );
            }
            return null;

        }
        else
        {
            Log.e( TAG, "Did not get a response from server." );
            return null;
        }
    }


    @Override
    protected void onPostExecute( List<String> result )
    {
        Log.d( TAG, "Got cookie is: " + result );
        if ( result!= null )
        {
            mCallback.itemCalled( result );
        }
        else
        {
            mCallback.itemCalled( mCookie );
        }
//        GlobalState state = ( ( CharadesApplication ) mCtx ).getGlobalState();
//        int requestTries = state.getEncodeJobTries();
//        if( result == null )
//        {
//            if ( requestTries > 0 )
//            {
//                // try again, we have 3 tries
//                state.setEncodeJobTries( requestTries - 1 );
//                Log.i( TAG, "Encoding Request failed. Trying it again." );
//                BuyItem newJob = new BuyItem( mCtx );
//                newJob.execute( mFilename );
//            }
//            else
//            {
//                // Give up
//                Log.e( TAG, "Already tried to send the start encode job 3 times and failed. Giving up :(" );
//            }
//        }
//        else
//        {
//            Log.i( TAG, "Elastic transcoder job was started." );
//        }
    }

}
