package com.projecta.hackathon.zalando.fragment;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;


import android.content.Context;

import android.os.Bundle;
import android.os.Vibrator;

import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager.LoaderCallbacks;
import android.support.v4.content.Loader;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnLongClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.projecta.hackathon.zalando.R;
import com.projecta.hackathon.zalando.loader.ArticleDetailsLoader;
import com.projecta.hackathon.zalando.models.Article;
import com.projecta.hackathon.zalando.models.ArticleDetails;
import com.projecta.hackathon.zalando.utils.Utils;

public class ImageFragment extends Fragment implements LoaderCallbacks< List<Object> >
{

    private ImageView           mProductImage;
    private static final String ARTICLE   = "article";
    private static final String TAG       = "ImageFragment";
    private static final String BASE_URL = "http://disrupt-hackathon.zalando.net/article/www.zalando.co.uk/";
    private static final int DETAIL_LOADER = 50;

    private ImageView           mPinImageView;

    private boolean             longpress = false;
    private int                 mParentId;
    private Article             mArticle;

    private Spinner             mSizeSpinner;
    private RelativeLayout      mContainer;

    public interface OnImageClick
    {
        void onImageClicked( final Article article, int adapterId );
    }

    public interface OnLongImageClick
    {
        void onLongImageClick( int id );
    }

    private OnLongImageClick mLongListener;

    private OnImageClick     mListener;

    public static ImageFragment newInstance( final Article article, int parentId )
    {
        ImageFragment f = new ImageFragment();
        f.setParentId( parentId );
        Bundle args = new Bundle();
        args.putParcelable( ARTICLE, article );
        f.setArguments( args );
        return f;
    }

    @Override
    public View onCreateView( LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState )
    {
        Bundle arguments = getArguments();
        mArticle = arguments.getParcelable( ARTICLE );

        View layout = inflater.inflate( R.layout.fragment_imageviewer, container, false );
        layout.setOnLongClickListener(new OnLongClickListener()
        {

            @Override
            public boolean onLongClick( View v )
            {
                tooglePinImg();
                mLongListener.onLongImageClick( getParentId() );
                return false;
            }
        });
        mSizeSpinner = (Spinner) layout.findViewById( R.id.size_spinner );
        mProductImage = ( ImageView ) layout.findViewById( R.id.imageView_zalando );
        mPinImageView = ( ImageView ) layout.findViewById( R.id.imagebtn_pin );
        mProductImage.setOnLongClickListener( new OnLongClickListener()
        {

            @Override
            public boolean onLongClick( View v )
            {
                tooglePinImg();
                mLongListener.onLongImageClick( getParentId() );
                return false;
            }
        });
        /*
         * mPinImgBtn.setOnClickListener( new OnClickListener() {
         *
         * @Override public void onClick( View v ) {
         * mLongListener.onLongImageClick( getParentId() ); } } );
         */
        // TODO start loading the image here...
        ImageLoader imageLoader = Utils.getConfiguredImageLoader( getActivity() );

        Log.d( TAG, "Displaying " + mArticle.getName() + " Url: " + mArticle.getImageUrl()
                + " Price: " + mArticle.getPrice() + " ColorFamily: " +mArticle.getColorFamily());
        imageLoader.displayImage( mArticle.getImageUrl(), mProductImage );
        getLoaderManager().initLoader( DETAIL_LOADER, null, this );
        return layout;
    }

    private void tooglePinImg()
    {
        Vibrator v = ( Vibrator ) getActivity().getSystemService( Context.VIBRATOR_SERVICE );
        v.vibrate( 100 );
        Log.d(TAG, "Visibility " + mPinImageView.getVisibility());
        if(mPinImageView.getVisibility() == View.VISIBLE)
        {
            mPinImageView.setVisibility( View.INVISIBLE );
        }
        else
        {
            mPinImageView.setVisibility( View.VISIBLE );
        }
    }

    @Override
    public void onSaveInstanceState( Bundle outState )
    {
        super.onSaveInstanceState( outState );
    }

    @Override
    public void onAttach( final Activity activity )
    {
        super.onAttach( activity );
        try
        {
            mListener = ( OnImageClick ) activity;
            mLongListener = ( OnLongImageClick ) activity;
        }
        catch ( ClassCastException exp )
        {
            Log.e( TAG, exp.getMessage() );
            throw new RuntimeException( "Your activity have to implement OnImageCLick interface" );
        }
    }

    public int getParentId()
    {
        return mParentId;
    }

    public void setParentId( int mParentId )
    {
        this.mParentId = mParentId;
    }

    public Article getArticle()
    {
        return mArticle;
    }

    public void setArticle( Article mArticle )
    {
        this.mArticle = mArticle;
    }

    @Override
    public Loader< List< Object >> onCreateLoader( int arg0, Bundle arg1 )
    {
        final String url = BASE_URL + mArticle.getSku();
        return new ArticleDetailsLoader( getActivity().getApplicationContext(), url ,  0);
    }


    @Override
    public void onLoadFinished( Loader< List< Object >> arg0, List< Object > arg1 )
    {
        List<String> mSizes = new ArrayList< String >();
        mArticle.setDetailsObjects( arg1 );
        for(ArticleDetails articleDetails : mArticle.getDetails())
        {
            Log.d(TAG, "Size: " + articleDetails.getSize());
            mSizes.add( articleDetails.getSize() );
        }

        ArrayAdapter< String > sizeAdapter = new ArrayAdapter< String >( getActivity(), R.layout.size_item,R.id.textView1, mSizes );
        mSizeSpinner.setAdapter( sizeAdapter);
    }

    @Override
    public void onLoaderReset( Loader< List< Object >> arg0 )
    {
        // TODO Auto-generated method stub

    }


}
