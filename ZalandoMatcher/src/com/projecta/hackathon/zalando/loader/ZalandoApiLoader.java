package com.projecta.hackathon.zalando.loader;

import java.lang.reflect.Type;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.json.JSONObject;

import android.content.AsyncTaskLoader;
import android.content.Context;
import android.util.Log;

import com.google.gson.reflect.TypeToken;
import com.projecta.hackathon.zalando.MainActivity;
import com.projecta.hackathon.zalando.ZalandoMatcherApplication;
import com.projecta.hackathon.zalando.http.HttpClientNew;
import com.projecta.hackathon.zalando.http.Response;
import com.projecta.hackathon.zalando.models.Article;
import com.projecta.hackathon.zalando.utils.Beans;
import com.projecta.hackathon.zalando.utils.Utils;

public class ZalandoApiLoader extends AsyncTaskLoader< List< Object > >
{
    private static final String TAG = "ZalandoApiLoader";
    private List< Object >     mData;
    private String              mSearchUri;

    public ZalandoApiLoader( Context context, String searchUri )
    {
        super( context );
        this.mSearchUri = searchUri;
    }

    @Override
    public List< Object > loadInBackground()
    {
        Log.i( TAG, "Load in background started!" );
        HttpClientNew client = new HttpClientNew();
        Map<String, List<String>> headers = new HashMap< String, List< String > >();
        headers.put( "Accept", Beans.toList( "application/json" ) );
        URL searchUrl = null;
        try
        {
            searchUrl = new URL( mSearchUri );
        }
        catch ( MalformedURLException e )
        {
            // TODO Auto-generated catch block
            Log.e( TAG, "Wrong url.", e );
        }
        Response response = client.get( searchUrl, headers );
        String body = new String(response.body);
        Log.d( TAG, "Got reponse: " + body );
        // Decode the JSON
        Type type = new TypeToken<Map<String, Object>>() {}.getType();
        Map<String, Object> map = ( Map<String, Object> ) Utils.jsonToObject( body, type );
        Log.d( TAG, "Decoded map: " + map );
        Map<String, Object > searchResults = ( Map< String, Object > ) map.get( "searchResults" );
        List<Map<String, Object>> data = ( List< Map< String, Object >> ) searchResults.get( "data" );

        List< Object > articles = new ArrayList< Object >();
        Set< String > colorFamilies = new HashSet< String >();
        for ( Map<String, Object> articleMap : data )
        {
            Article article = new Article();
            article.setImageUrl( ( String ) articleMap.get( "imageUrl" ) );
            article.setBrandCode( ( String ) articleMap.get( "brandCode" ) );
            article.setColor( ( String ) articleMap.get( "color" ) );
            article.setColorFamily( ( String ) articleMap.get( "colorFamily" ) );
            article.setSku( ( String ) articleMap.get( "sku" ) );
            article.setName( ( String ) articleMap.get( "name" ) );
            article.setUrlKey( ( String ) articleMap.get( "urlKey" ) );
            article.setPrice( ( Double ) articleMap.get( "price" ) );
            article.setKids( ( Boolean ) articleMap.get( "kids" ) );
            articles.add( article );
            colorFamilies.add( article.getColorFamily() );
        }
   
        switch ( getId() )
        {
            case MainActivity.TOP:
                putToMap(((ZalandoMatcherApplication) getContext().getApplicationContext()).getCfTops(), articles);
                break;
                
            case MainActivity.MIDDLE:
                putToMap(((ZalandoMatcherApplication) getContext().getApplicationContext()).getCfPants(), articles);
                break;
            case MainActivity.BOTTOM:
                putToMap(((ZalandoMatcherApplication) getContext().getApplicationContext()).getCfShoes(), articles);
                break;
            default:
                break;
        }

        Log.d( TAG, "Families: " + colorFamilies );

        return articles;
    }

    private void putToMap( Map< String, List< Article >> map, List<Object> articles )
    {
        for(Object article : articles)
        {
            final String key = ((Article)article).getColorFamily();
            if(map.containsKey( key))
            {
                map.get( key ).add( (Article)article );
            }
            else
            {
                ArrayList< Article > articleList = new ArrayList< Article >();
                articleList.add( (Article)article );
                map.put( key, articleList);
            }
        }
    }

    /**
     * Called when there is new data to deliver to the client. The superclass
     * will deliver it to the registered listener (i.e. the LoaderManager),
     * which will forward the results to the client through a call to
     * onLoadFinished.
     */
    @Override
    public void deliverResult( List< Object > data )
    {
        if ( isReset() )
        {
            Log.w( TAG, "Warning! An async query came in while the Loader was reset!" );
            // The Loader has been reset; ignore the result and invalidate the
            // data. This can happen when the Loader is reset while an
            // asynchronous query is working in the background. That is, when
            // the background thread finishes its work and attempts to deliver
            // the results to the client, it will see here that the Loader has
            // been reset and discard any resources associated with the new data
            // as necessary.
            if ( data != null )
            {
                releaseResources( data );
                return;
            }
        }

        // Hold a reference to the old data so it doesn't get garbage collected.
        // We must protect it until the new data has been delivered.
        List< Object > oldData = mData;
        this.mData = data;

        if ( isStarted() )
        {
            Log.i( TAG, "Delivering results to the LoaderManager to display!" );
            // If the Loader is in a started state, have the superclass deliver
            // the results to the client.
            super.deliverResult( data );
        }

        // Invalidate the old data as we don't need it any more.
        if ( oldData != null && oldData != data )
        {
            Log.i( TAG, "Releasing any old data associated with this Loader." );
            releaseResources( oldData );
        }
    }

    @Override
    protected void onStartLoading()
    {
        Log.i( TAG, "onStartLoading() called!" );

        if ( this.mData != null )
        {
            // Deliver any previously loaded data immediately.
            Log.i( TAG, "Delivering previously loaded data to the client..." );
            deliverResult( this.mData );
        }

        // Register the observers that will notify the Loader when changes are
        // made.

        // TODO: Register PushNotificationObservers
        // if (mAppsObserver == null) {
        // mAppsObserver = new InstalledAppsObserver(this);
        // }

        if ( takeContentChanged() )
        {
            // When the observer detects a new installed application, it will
            // call onContentChanged() on the Loader, which will cause the next
            // call to takeContentChanged() to return true. If this is ever the
            // case (or if the current data is null), we force a new load.
            Log.i( TAG, "A content change has been detected... so force load!" );
            forceLoad();
        }
        else if ( mData == null )
        {
            // If the current data is null... then we should make it non-null!
            Log.i( TAG, "The current data is data is null... so force load!" );
            forceLoad();
        }

        // Register the observers that will notify the Loader when changes are
        // made.
        // if ( mObserver == null )
        // {
        // mObserver = new ParseGameObserver( this );
        // }
    }

    @Override
    protected void onStopLoading()
    {
        Log.i( TAG, "onStopLoading() called!" );

        // The Loader has been put in a stopped state, so we should attempt to
        // cancel the current load (if there is one).
        cancelLoad();
        // Note that we leave the observer as is; Loaders in a stopped state
        // should still monitor the data source for changes so that the Loader
        // will know to force a new load if it is ever started again.
    }

    @Override
    protected void onReset()
    {
        Log.i( TAG, "onReset() called!" );

        // Ensure the loader is stopped.
        onStopLoading();

        // At this point we can release the resources associated with 'apps'.
        if ( mData != null )
        {
            releaseResources( mData );
            this.mData = null;
        }

        // The Loader is being reset, so we should stop monitoring for changes.
        // if (mAppsObserver != null) {
        // getContext().unregisterReceiver(mAppsObserver);
        // mAppsObserver = null;
        // }
    }

    @Override
    public void onCanceled( List< Object > data )
    {
        Log.i( TAG, "onCanceled() called!" );

        // Attempt to cancel the current asynchronous load.
        super.onCanceled( data );

        // The load has been canceled, so we should release the resources
        // associated with 'mData'.
        releaseResources( data );
    }

    @Override
    public void forceLoad()
    {
        Log.i( TAG, "forceLoad() called!" );
        super.forceLoad();
    }

    /**
     * Helper method to take care of releasing resources associated with an
     * actively loaded data set.
     */
    private void releaseResources( List< Object > data )
    {
        // For a simple List, there is nothing to do. For something like a
        // Cursor, we would close it in this method. All resources associated
        // with the Loader should be released here.
    }

}
