package com.projecta.hackathon.zalando;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.app.Activity;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;
import android.webkit.WebView;
import android.webkit.WebViewClient;

public class WebviewActivity extends Activity
{

    private final static String TAG = "Webview";

    private WebView             webView;

    @Override
    public void onCreate( Bundle savedInstanceState )
    {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_webview );

        //webView = ( WebView ) findViewById( R.id.webView );
        webView = new WebView( getApplicationContext() );

//        List< String > cookie = getIntent().getExtras().getStringArrayList( "Cookie" );
        String lastUrl = getIntent().getExtras().getString( "url" );
//        Map< String, String > headers = new HashMap< String, String >();
//        String cookieStr = "";
//        for ( String cook: cookie )
//        {
//            cookieStr += cook + ",";
//        }
//        headers.put( "Cookie", cookieStr );
//        Log.d( TAG, "Headers: " + headers );
        webView.getSettings().setJavaScriptEnabled( true );
//        webView.loadUrl( lastUrl, headers );
        webView.loadUrl( lastUrl);


    }

    public class myWebClient extends WebViewClient
    {
        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            // TODO Auto-generated method stub
            super.onPageStarted(view, url, favicon);
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            // TODO Auto-generated method stub

            view.loadUrl(url);
            return true;

        }
    }
}
