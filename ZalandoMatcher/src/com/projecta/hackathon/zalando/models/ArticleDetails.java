package com.projecta.hackathon.zalando.models;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;

public class ArticleDetails implements Parcelable
{

    private String  addToBasketUrl;
    private String  size;
    private String  detailSku;
    private Boolean available;

    public ArticleDetails(){}

    @Override
    public String toString()
    {
        return "ArticleDetails [addToBasketUrl=" + addToBasketUrl + ", size=" + size
            + ", detailSku=" + detailSku + ", available=" + available + "]";
    }

    private ArticleDetails( Parcel in )
    {
        this.addToBasketUrl = in.readString();
        this.size = in.readString();
        this.detailSku = in.readString();
        this.available = ( in.readInt() == 1 );
    }

    public String getAddToBasketUrl()
    {
        return addToBasketUrl;
    }

    public void setAddToBasketUrl( String addToBasketUrl )
    {
        this.addToBasketUrl = addToBasketUrl;
    }

    public String getSize()
    {
        return size;
    }

    public void setSize( String size )
    {
        this.size = size;
    }

    public String getDetailSku()
    {
        return detailSku;
    }

    public void setDetailSku( String detailSku )
    {
        this.detailSku = detailSku;
    }

    public Boolean isAvailable()
    {
        return available;
    }

    public void setAvailable( Boolean available )
    {
        this.available = available;
    }

    @Override
    public int describeContents()
    {
        return 0;
    }

    @Override
    public void writeToParcel( Parcel dest, int flags )
    {
        dest.writeString( getAddToBasketUrl() );
        dest.writeString( getSize() );
        dest.writeString( getDetailSku() );
        dest.writeInt( isAvailable() ? 1: 0 );
    }

    public static final Creator<ArticleDetails> CREATOR = new Creator<ArticleDetails>() {
        @Override
        public ArticleDetails createFromParcel(Parcel in) {
            return new ArticleDetails(in);
        }
        @Override
        public ArticleDetails[] newArray(int size) {
            return new ArticleDetails[size];
        }
    };


}
