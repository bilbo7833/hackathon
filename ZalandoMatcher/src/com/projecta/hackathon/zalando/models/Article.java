package com.projecta.hackathon.zalando.models;

import java.util.ArrayList;
import java.util.List;

import android.os.Parcel;
import android.os.Parcelable;

import com.projecta.hackathon.zalando.utils.Beans;

/**
 * Represents the article from zalando
 *
 * @author bahlfeld
 *
 */
public class Article implements Parcelable
{

    @Override
    public String toString()
    {
        return "Article [brandCode=" + brandCode + ", color=" + color + ", colorFamily="
            + colorFamily + ", name=" + name + ", sku=" + sku + ", urlKey=" + urlKey
            + ", imageUrl=" + imageUrl + ", kids=" + kids + ", newArticle=" + newArticle

            + ", oversize=" + oversize + ", sale=" + sale + ", price=" + price + ", priceOriginal="
            + priceOriginal + "]";
    }

    private String                 brandCode, color, colorFamily, name, sku, urlKey, imageUrl;

    private boolean                kids, newArticle, oversize, sale;

    private double                 price, priceOriginal;

    private List< ArticleDetails > details = new ArrayList< ArticleDetails >();

    public Article()
    {

    }

    private Article( Parcel in )
    {
        this.brandCode = in.readString();
        this.color = in.readString();
        this.colorFamily = in.readString();
        this.name = in.readString();
        this.sku = in.readString();
        this.urlKey = in.readString();
        this.imageUrl = in.readString();
        this.kids = in.readInt() == 1 ? true : false;
        this.newArticle = in.readInt() == 1 ? true : false;
        this.oversize = in.readInt() == 1 ? true : false;
        this.sale = in.readInt() == 1 ? true : false;
        this.details = Beans.toList( ( ArticleDetails[] ) in.readParcelableArray( ArticleDetails.class
                    .getClassLoader() ) );
    }

    public String getBrandCode()
    {
        return brandCode;
    }

    public void setBrandCode( String brandCode )
    {
        this.brandCode = brandCode;
    }

    public String getColor()
    {
        return color;
    }

    public void setColor( String color )
    {
        this.color = color;
    }

    public String getColorFamily()
    {
        return colorFamily;
    }

    public void setColorFamily( String colorFamily )
    {
        this.colorFamily = colorFamily;
    }

    public String getName()
    {
        return name;
    }

    public void setName( String name )
    {
        this.name = name;
    }

    public String getSku()
    {
        return sku;
    }

    public void setSku( String sku )
    {
        this.sku = sku;
    }

    public String getUrlKey()
    {
        return urlKey;
    }

    public void setUrlKey( String urlKey )
    {
        this.urlKey = urlKey;
    }

    public boolean isKids()
    {
        return kids;
    }

    public void setKids( boolean kids )
    {
        this.kids = kids;
    }

    public boolean isNewArticle()
    {
        return newArticle;
    }

    public void setNewArticle( boolean newArticle )
    {
        this.newArticle = newArticle;
    }

    public boolean isOversize()
    {
        return oversize;
    }

    public void setOversize( boolean oversize )
    {
        this.oversize = oversize;
    }

    public boolean isSale()
    {
        return sale;
    }

    public void setSale( boolean sale )
    {
        this.sale = sale;
    }

    public double getPrice()
    {
        return price;
    }

    public void setPrice( double price )
    {
        this.price = price;
    }

    public double getPriceOriginal()
    {
        return priceOriginal;
    }

    public void setPriceOriginal( double priceOriginal )
    {
        this.priceOriginal = priceOriginal;
    }

    public List< ArticleDetails > getDetails()
    {
        return details;
    }

    public void setDetails( List< ArticleDetails > details )
    {
        this.details = details;
    }

    public void setDetailsObjects( List< Object > details )
    {
        this.details = new ArrayList<ArticleDetails>();
        for ( Object obj: details )
        {
            this.details.add( (ArticleDetails) obj );
        }
    }

    public String getImageUrl()
    {
        return imageUrl;
    }

    public void setImageUrl( String imageUrl )
    {
        this.imageUrl = imageUrl;
    }

    @Override
    public int describeContents()
    {

        return 0;
    }

    @Override
    public void writeToParcel( Parcel dest, int flags )
    {
        dest.writeString( getBrandCode() );
        dest.writeString( getColor() );
        dest.writeString( getColorFamily() );
        dest.writeString( getImageUrl() );
        dest.writeString( getName() );
        dest.writeString( getSku() );
        dest.writeString( getUrlKey() );
        dest.writeDouble( getPrice() );
        dest.writeDouble( getPriceOriginal() );
        dest.writeInt( isKids() ? 1 : 0 );
        dest.writeInt( isNewArticle() ? 1 : 0 );
        dest.writeInt( isOversize() ? 1 : 0 );
        dest.writeInt( isSale() ? 1 : 0 );
        ArticleDetails[] arr = new ArticleDetails[getDetails().size()];
        ArticleDetails[] arr1 = getDetails().toArray( arr );
        dest.writeParcelableArray( arr1, 0 );
    }

    public static final Creator< Article > CREATOR = new Creator< Article >()
   {
       @Override
       public Article createFromParcel( Parcel in )
       {
           return new Article( in );
       }

       @Override
       public Article[] newArray( int size )
       {
           return new Article[ size ];
       }
   };

    

}
