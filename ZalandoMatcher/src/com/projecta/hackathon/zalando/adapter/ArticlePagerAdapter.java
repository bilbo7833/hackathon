package com.projecta.hackathon.zalando.adapter;

import java.util.List;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.util.Log;

import com.projecta.hackathon.zalando.fragment.ImageFragment;
import com.projecta.hackathon.zalando.models.Article;

public class ArticlePagerAdapter extends FragmentPagerAdapter
{

    private static final String TAG = "ArticlePagerAdapter";
    private List< Object >     mArticles;
    private int                 mId;

    public ArticlePagerAdapter( FragmentManager fm, int id, List< Object > urls )
    {
        super( fm );
        mArticles = urls;
        mId = id;
    }

    public void setData( List< Object > newData )
    {
        for(Object obj : newData)
        {
            Log.d(TAG,((Article)obj).getColorFamily());
        }
        if(mArticles != null)
        {
            this.mArticles.clear();
        }
        this.mArticles = newData;
        this.notifyDataSetChanged();
        
    }

    @Override
    public Fragment getItem( int pos )
    {
        Log.d( TAG, "URL: " + mArticles.get( pos ) );
        return ImageFragment.newInstance( ( Article ) mArticles.get( pos ), mId );
    }

    public Object getArticle( int pos )
    {
        return mArticles.get( pos );
    }


    @Override
    public int getCount()
    {
        if ( mArticles != null )
        {
            //Log.d( TAG, "Size " + mArticles.size() );
            return mArticles.size();
        }
        else
        {
            //Log.d( TAG, "Size is 0" );
            return 0;
        }
    }
    @Override
    public int getItemPosition(Object object) {
        return POSITION_NONE;
    }
}
