package com.projecta.hackathon.zalando.utils;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;

import android.net.Uri;
import android.util.Log;

// TODO: Import packages direct from the JAVA Project A Framework per Maven

/**
 * General utility class for handling Java objects
 *
 * @author kuehnel
 */
public final class Beans {

    private static String TAG = "Beans";

    private static Random rand = new Random();

    /**
     * Compares if two beans are equal. Takes null values into account
     */
    public static boolean equals( Object o1, Object o2 ) {

        if ( o1 == o2 ) {
            return true;
        }
        if ( o1 == null || o2 == null ) {
            return false;
        }

        if ( o1.getClass() != o2.getClass()
                && !o1.getClass().isAssignableFrom( o2.getClass() )
                && !o2.getClass().isAssignableFrom( o1.getClass() ) ) {
            RuntimeException e = new RuntimeException();
            e.fillInStackTrace();
            Log.e( TAG, "Beans.equals: Comparing objects of type " + o1.getClass().getName()
                     + " and " + o2.getClass().getName()+" ("+o1+" = "+o2+")", e );
        }

        return o1.equals( o2 );
    }


    /**
     * Compares if two beans are not equal. Takes null values into account
     */
    public static boolean notEquals( Object o1, Object o2 ) {

        if ( o1 == o2 ) {
            return false;
        }
        if ( o1 == null || o2 == null ) {
            return true;
        }

        if ( o1.getClass() != o2.getClass()
                && !o1.getClass().isAssignableFrom( o2.getClass() )
                && !o2.getClass().isAssignableFrom( o1.getClass() ) ) {
            RuntimeException e = new RuntimeException();
            e.fillInStackTrace();
            Log.e( TAG,"Beans.notEquals: Comparing objects of type " + o1.getClass().getName()
                     + " and " + o2.getClass().getName()+" ("+o1+" <> "+o2+")", e );
        }

        return !o1.equals( o2 );
    }


    /**
     * Retrieves the hashCode of an object, or 0 if the object was null
     */
    public static int hashCode( Object object ) {
        return object == null ? 0 : object.hashCode();
    }

    /**
     * Returns either the first object, or if that is null, the second one
     */
    public static<T> T either( T o1, T o2 ) {
        return (o1 == null) ? o2 : o1;
    }

    /** Utility method for accessing null Doubles */
    public static double nz( Double d ) {
        return d != null ? d : 0;
    }

    /** Utility method for accessing null Integers */
    public static int nz( Integer i ) {
        return i != null ? i : 0;
    }

    /** Utility method for accessing null Longs */
    public static long nz( Long l ) {
        return l != null ? l : 0;
    }

    /** Utility method for accessing null Floats */
    public static float nz( Float f ) {
        return f != null ? f : 0f;
    }

    /** Utility method for accessing null Booleans */
    public static boolean nz( Boolean b ) {
        return b != null ? b : false;
    }

    /** Utility method for encoding null Uris */
    public static Uri StrToUri( String u ) {
        return u != null ? Uri.parse( u ) : null;
    }

    /** Utility method for decoding null Uris */
    public static String UriToStr( Uri u ) {
        return u != null ? u.toString() : null;
    }

    public static Integer DToI( Double d ) {
        return d != null ? d.intValue() : null;
    }

    public static Double IToD( Integer i ) {
        return i != null ? i.doubleValue() : null;
    }


    /** Utility method for iterating over null Lists */
    public static<T> List<T> nz( List<T> list ) {
        return list != null ? list : Collections.<T>emptyList();
    }

    /** Utility method for iterating over null Sets */
    public static<T> Set<T> nz( Set<T> set ) {
        return set != null ? set : Collections.<T>emptySet();
    }

    /** Utility method for iterating over null Collections */
    public static<T> Collection<T> nz( Collection<T> list ) {
        return list != null ? list : Collections.<T>emptyList();
    }

    /** Utility method for iterating over null Maps */
    public static <K, V> Map<K, V> nz(Map<K, V> map) {
        return map != null ? map : Collections.<K, V>emptyMap();
    }

    /**
     * Converts a list of key value pairs to a map
     */
    @SuppressWarnings("unchecked")
    public static<K,V> Map<K,V> toMap( Object... values )
    {
        LinkedHashMap<K,V> map = new LinkedHashMap<K,V>( values.length / 2 );
        for ( int i = 0; i < values.length; i += 2 ) {
            map.put( (K) values[i], (V) values[i+1] );
        }
        return map;
    }

    /** Utility method for creating a constant set */
    public static<T> Set<T> toSet( T... array ) {
        return new HashSet<T>( Arrays.asList( array ) );
    }

    /** Utility method for creating a constant list */
    public static<T> List<T> toList( T... array ) {
        return Arrays.asList( array );
    }

    /** Returns true, if a collection is empty */
    public static boolean isEmpty( Collection<?> coll ) {
        return coll == null || coll.isEmpty();
    }

    /** Returns true, if a collection is empty */
    public static boolean isEmpty( Map<?,?> map ) {
        return map == null || map.isEmpty();
    }

    /** Returns true, if an array is empty */
    public static<T> boolean isEmpty( T[] array ) {
        return array == null || array.length == 0;
    }

    /** Returns true, if a collection is not empty */
    public static boolean isNotEmpty( Collection<?> coll ) {
        return coll != null && !coll.isEmpty();
    }

    /** Returns true, if an array is not empty */
    public static<T> boolean isNotEmpty( T[] array ) {
        return array != null && array.length != 0;
    }

    /**
     * Sleeps for a given amount of milliseconds
     */
    public static void sleep( long milliseconds ) {
        try {
            Thread.sleep( milliseconds );
        }
        catch ( InterruptedException e ) {
            // ignore
        }
    }


    /**
     * Sleeps for a random amount of milliseconds in the given range
     */
    public static void sleep( int minMilliseconds, int maxMilliseconds ) {
        try {
            int sleepTime = minMilliseconds + rand.nextInt( maxMilliseconds-minMilliseconds );
            Thread.sleep( sleepTime );
        }
        catch ( InterruptedException e ) {
            // ignore
        }
    }

    /**
     * Returns true, if an element of the collection is null.
     */
    public static <T> boolean containsNullElement( Collection<T> coll) {
        for (T elem: coll) {
            if (elem == null) {
                return true;
            }
        }
        return false;
    }

    /**
     * Sets all elements in the Collection to null.
     */
    public static <T> void setAllToNull( Collection<T> coll) {
        for (T elem: coll) {
            elem = null;
        }
    }

}
