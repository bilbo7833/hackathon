package com.projecta.hackathon.zalando.utils;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import android.graphics.Bitmap;
import android.net.Uri;
import android.util.Log;

public class IOUtils
{

    private static final String TAG              = "IOUtils";

    static class FlushedInputStream extends FilterInputStream
    {
        public FlushedInputStream( InputStream inputStream )
        {
            super( inputStream );
        }

        @Override
        public long skip( long n ) throws IOException
        {
            long totalBytesSkipped = 0L;
            while ( totalBytesSkipped < n )
            {
                long bytesSkipped = in.skip( n - totalBytesSkipped );
                if ( bytesSkipped == 0L )
                {
                    int byt = read();
                    if ( byt < 0 )
                    {
                        break; // we reached EOF
                    }
                    else
                    {
                        bytesSkipped = 1; // we read one byte
                    }
                }
                totalBytesSkipped += bytesSkipped;
            }
            return totalBytesSkipped;
        }
    }
    /**
     * Copies a file from sourcePath to destinationPath
     *
     * @param sourcePath
     * @param destinationPath
     * @return
     */
    public static boolean copyfile( String sourcePath, String destinationPath )
    {
        File src = new File( sourcePath );
        File dst = new File( destinationPath );
        InputStream in;
        OutputStream out;
        try
        {
            in = new FileInputStream( src );
            out = new FileOutputStream( dst );
        }
        catch ( FileNotFoundException e )
        {
            e.printStackTrace();
            return false;
        }
        return copyStream( in, out );
    }

    /**
     * Writes a stream to file
     *
     * @param in
     * @param file
     * @return
     */
    public static void streamToFile( BufferedInputStream in, File file ) throws IOException
    {
        Log.d( TAG, "Writing input response stream to file: " + file.getAbsolutePath() );
        BufferedOutputStream output = null;
        try
        {
            output = new BufferedOutputStream( new FileOutputStream( file ) );
        }
        catch ( FileNotFoundException e )
        {
            Log.e( TAG, "Could not open file.", e );
        }
        if ( output != null )
        {
            try
            {
                byte[] buffer = new byte[ 1024 ];
                int bytesRead = 0;
                while ( ( bytesRead = in.read( buffer ) ) >= 0 )
                {
                    output.write( buffer, 0, bytesRead );
                }
            }
            finally
            {
                output.close();
            }
        }
    }

    /**
     * Reads the bytes from an inputStream
     *
     * @param in
     * @return byte[]
     */
    public static byte[] bytesFromStream( InputStream in ) throws IOException
    {
        byte[] buf = new byte[ 1024 ];
        int count = 0;
        ByteArrayOutputStream out = new ByteArrayOutputStream( 1024 );
        while ( ( count = in.read( buf ) ) != -1 )
            out.write( buf, 0, count );
        return out.toByteArray();
    }

    public static void deleteFilesAtPath( File parentDir )
    {
        File[] files = parentDir.listFiles();
        if ( files != null && files.length > 0 )
        {
            for ( File file: files )
            {
                if ( file.getName().endsWith( ".png" ) )
                {
                    File myFile = new File( parentDir + "/" + file.getName() );
                    myFile.delete();
                }
            }
        }
    }

    public static boolean saveBitmapToStorage( Bitmap bitmap, Uri location )
    {
        if ( bitmap == null )
        {
            Log.e( TAG, "Trying to write null bitmap to " + location );
            return false;
        }
        Log.d( TAG, "Writing bitmap to " + location );
        File file = new File( location.getPath() );
        String parent = file.getParent();
        if ( !FilesystemUtils.assurePathExists( new File( parent ) ) )
        {
            Log.e( TAG, "Failed to create folder " + parent );
        }
        else
        {
            BufferedOutputStream fos;
            try
            {
                fos = new BufferedOutputStream( new FileOutputStream( file ) );
                bitmap.compress( Bitmap.CompressFormat.PNG, 100, fos );
                fos.flush();
                fos.close();
                return true;
            }
            catch ( FileNotFoundException e )
            {
                e.printStackTrace();
            }
            catch ( IOException e )
            {
                e.printStackTrace();
            }
        }
        return false;
    }

//    public static byte[] getByteArrayFromUri( Context ctx, Uri uri ) throws FileNotFoundException
//    {
//        InputStream in = null;
//        byte[] result = null;
//        in = ctx.getContentResolver().openInputStream( uri );
//        if ( in != null )
//        {
//            try
//            {
//                result = com.devsmart.android.IOUtils.toByteArray( in );
//            }
//            catch ( IOException e )
//            {
//                Log.e( TAG, "Could not bytes from stream for file " + uri, e );
//            }
//        }
//        return result;
//    }

    /**
     * Buffered copy of an inputStream into an outputStream
     *
     * @param inStream
     * @param outStream
     * @return
     */
    public static boolean copyStream( InputStream inStream, OutputStream outStream )
    {
        byte[] buf = new byte[ 1024 ];
        int len;
        try
        {
            while ( ( len = inStream.read( buf ) ) > 0 )
            {
                outStream.write( buf, 0, len );
            }
            inStream.close();
            outStream.close();
        }
        catch ( FileNotFoundException ex )
        {
            ex.printStackTrace();
            return false;
        }
        catch ( IOException e )
        {
            e.printStackTrace();
            return false;
        }
        return true;
    }
}
