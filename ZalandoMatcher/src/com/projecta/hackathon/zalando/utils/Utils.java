package com.projecta.hackathon.zalando.utils;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Type;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.ActivityManager.RunningServiceInfo;
import android.app.AlertDialog;
import android.app.Service;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.pm.ResolveInfo;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Debug;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.Settings.Secure;
import android.telephony.TelephonyManager;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.ViewGroup;

import com.google.gson.Gson;
import com.google.gson.JsonParseException;
import com.nostra13.universalimageloader.cache.memory.impl.UsingFreqLimitedMemoryCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.display.SimpleBitmapDisplayer;


/**
 * @author stefananca
 *
 */
// TODO: Refactor this mess
public class Utils
{

    private static String TAG = "Utils";

    @SuppressLint( "NewApi" )
    public static int heapSize( Activity ctx )
    {
        ActivityManager am = ( ActivityManager ) ctx.getSystemService( Service.ACTIVITY_SERVICE );
        if ( Build.VERSION.SDK_INT >= 11 )
        {
            return am.getLargeMemoryClass();
        }
        else
        {
            return am.getMemoryClass();
        }
    }

    @SuppressWarnings( "deprecation" )
    public static int getDefaultPixelFormat( Activity ctx )
    {
        Display defaultDisplay = ctx.getWindowManager().getDefaultDisplay();
        return defaultDisplay.getPixelFormat();
    }

    public static String md5Hash( byte[] dataBytes )
    {
        MessageDigest md;
        try
        {
            md = MessageDigest.getInstance( "MD5" );
        }
        catch ( NoSuchAlgorithmException e )
        {
            Log.e( TAG, "No MD5 Algorithm is possible.", e );
            return null;
        }
        md.update( dataBytes );
        byte[] mdbytes = md.digest();
        StringBuilder strBuild = new StringBuilder();
        for ( int i = 0; i < mdbytes.length; i++ )
        {
            strBuild.append( Integer.toString( ( mdbytes[ i ] & 0xff ) + 0x100, 16 ).substring( 1 ) );
        }
        String hash = strBuild.toString();
        Log.d( TAG, "Computed the MD5 algorithm of " + dataBytes.length + " bytes: " + hash );
        return hash;
    }

    public static boolean isServiceRunning( Context ctx, Class serviceClass )
    {
        ActivityManager manager =
                ( ActivityManager ) ctx.getSystemService( Context.ACTIVITY_SERVICE );
        for ( RunningServiceInfo service: manager.getRunningServices( Integer.MAX_VALUE ) )
        {
            if ( serviceClass.getName().equals( service.service.getClassName() ) )
            {
                return true;
            }
        }
        return false;
    }

    public static void copyLicenseFromAssetToFolder( final Context ctx )
    {
        try
        {
            FilesystemUtils.assurePathExists( new File( Environment.getExternalStorageDirectory()
                    .getAbsolutePath()
                    + "/videokit" ) );
            if ( !FilesystemUtils.pathExists( new File( Environment.getExternalStorageDirectory()
                    .getAbsolutePath()
                    + "/videokit/ffmpeglicense.lic" ) ) )
            {
                Log.i( TAG, "License file doesn't exist at: "
                        + Environment.getExternalStorageDirectory().getAbsolutePath()
                        + "/videokit/ffmpeglicense.lic try to copy from assets" );
                InputStream is = ctx.getAssets().open( "ffmpeglicense.lic" );
                BufferedOutputStream o = null;
                try
                {
                    byte[] buff = new byte[ 10000 ];
                    int read = -1;
                    o =
                            new BufferedOutputStream( new FileOutputStream( Environment
                                    .getExternalStorageDirectory().getAbsolutePath()
                                    + "/videokit/ffmpeglicense.lic" ), 10000 );
                    while ( ( read = is.read( buff ) ) > -1 )
                    {
                        o.write( buff, 0, read );
                    }
                }
                finally
                {
                    is.close();
                    if ( o != null )
                        o.close();

                }
            }
        }
        catch ( IOException e )
        {
            Log.e( TAG, e.getMessage() );
        }
    }

    public static String getCurrentTimestamp( Context ctx )
    {
        return new SimpleDateFormat( "yyyyMMdd_HHmmss", CountryUtils.getLocale( ctx ) )
                .format( new Date() );
    }

    public static void unbindDrawables( View view )
    {
        if ( view.getBackground() != null )
        {
            view.getBackground().setCallback( null );
        }
        if ( view instanceof ViewGroup )
        {
            for ( int i = 0; i < ( ( ViewGroup ) view ).getChildCount(); i++ )
            {
                unbindDrawables( ( ( ViewGroup ) view ).getChildAt( i ) );
            }

            try
            {
                ( ( ViewGroup ) view ).removeAllViews();
            }
            catch ( UnsupportedOperationException mayHappen )
            {
                // AdapterViews, ListViews and potentially other ViewGroups
                // don’t support the removeAllViews operation
            }
        }
    }

    public static void logMemoryInfo( Context ctx )
    {
        Runtime rt = Runtime.getRuntime();
        long maxMemory = rt.maxMemory();
        Log.v( "onCreate", "maxMemory:" + Long.toString( maxMemory ) );

        ActivityManager am = ( ActivityManager ) ctx.getSystemService( Context.ACTIVITY_SERVICE );
        int memoryClass = am.getMemoryClass();
        Log.v( "onCreate", "memoryClass:" + Integer.toString( memoryClass ) );
    }

    // /*
    // * Free Memory in Android VM
    // */
    // public static long freeMemory()
    // {
    // long maxMemory = Runtime.getRuntime().maxMemory();
    // long nativeAllocationSize = Debug.getNativeHeapAllocatedSize();
    //
    // return maxMemory - nativeAllocationSize;
    // }

    public static int getAppVersion( Context ctx )
    {
        int versionCode = -1;
        try
        {
            PackageInfo info = ctx.getPackageManager().getPackageInfo( ctx.getPackageName(), 0 );
            versionCode = info.versionCode;
        }
        catch ( NameNotFoundException e )
        {
            e.printStackTrace();
        }
        return versionCode;
    }

    // Determine screen size
    public static String getScreenSize( Activity ctx )
    {
        String screenSize = "";
        if ( ( ctx.getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK ) == Configuration.SCREENLAYOUT_SIZE_XLARGE )
        {
            screenSize = "Extra Large screen";
        }
        else if ( ( ctx.getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK ) == Configuration.SCREENLAYOUT_SIZE_LARGE )
        {
            screenSize = "Large screen";
        }
        else if ( ( ctx.getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK ) == Configuration.SCREENLAYOUT_SIZE_NORMAL )
        {
            screenSize = "Normal sized screen";
        }
        else if ( ( ctx.getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK ) == Configuration.SCREENLAYOUT_SIZE_SMALL )
        {
            screenSize = "Small sized screen";
        }
        else if ( ( ctx.getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK ) == Configuration.SCREENLAYOUT_SIZE_UNDEFINED )
        {
            screenSize = "Undefined size screen";
        }
        else
        {
            screenSize = "Screen size is a complete mystery";
        }
        return screenSize;
    }

    public static String getDeviceInfo( Activity ctx )
    {
        String report = "";
        TelephonyManager manager =
                ( TelephonyManager ) ctx.getSystemService( Context.TELEPHONY_SERVICE );
        String carrierName = manager.getNetworkOperatorName();
        int versionCode = getAppVersion( ctx );
        String deviceId = Secure.getString( ctx.getContentResolver(), Secure.ANDROID_ID );
        String timestamp = new SimpleDateFormat( "yyyy-MM-dd HH:mm:ss.SSS" ).format( new Date() );
        DisplayMetrics metrics = new DisplayMetrics();
        ctx.getWindowManager().getDefaultDisplay().getMetrics( metrics );
        Runtime rt = Runtime.getRuntime();
        String maxMemory = Long.toString( rt.maxMemory() );
        int memoryClass = heapSize( ctx );
        long heapSize = Debug.getNativeHeapSize();
        long allocatedSize = Debug.getNativeHeapAllocatedSize();
        report += "-------------------------------------\n";
        report += "------------ Application ------------\n";
        report += "Name: " + ctx.toString() + "\n";
        report += "Version: " + versionCode + "\n";
        report += "Carrier: " + carrierName + "\n";
        report += "Time: " + timestamp + "\n";
        report += "-------------------------------------\n\n";
        report += "-------------- Device ---------------\n";
        report += "Brand: " + Build.BRAND + "\n";
        report += "Device: " + Build.DEVICE + "\n";
        report += "Model: " + Build.MODEL + "\n";
        report += "Id: " + Build.ID + "\n";
        report += "Product: " + Build.PRODUCT + "\n";
        report += "Unique Id: " + deviceId + "\n";
        report += "Height Px: " + metrics.heightPixels + "\n";
        report += "Width Px: " + metrics.widthPixels + "\n";
        report += "Screen Size: " + getScreenSize( ctx ) + "\n";
        report += "Density: " + metrics.density + "\n";
        report += "Scaled Density: " + metrics.scaledDensity + "\n";
        report += "DensityDPI: " + metrics.densityDpi + "\n";
        report += "MaxMemory: " + maxMemory + "\n";
        report += "MemoryClass: " + memoryClass + " MB\n";
        report += "Native Heap Size: " + Long.toString( heapSize ) + "\n";
        report += "Allocated Size: " + Long.toString( allocatedSize ) + "\n";
        report += "-------------------------------------\n\n";
        report += "------------- Firmware --------------\n";
        report += "SDK: " + Build.VERSION.SDK + "\n";
        report += "Release: " + Build.VERSION.RELEASE + "\n";
        report += "Incremental: " + Build.VERSION.INCREMENTAL + "\n";
        report += "-------------------------------------\n";
        return report;

    }

    public static boolean isIntentAvailable( Context context, String action )
    {
        final PackageManager packageManager = context.getPackageManager();
        final Intent intent = new Intent( action );
        List< ResolveInfo > list =
                packageManager.queryIntentActivities( intent, PackageManager.MATCH_DEFAULT_ONLY );
        return list.size() > 0;
    }


    public static String getRealPathFromURI( final Uri contentUri, final Activity act )
    {
        String[] proj = { MediaStore.Images.Media.DATA };
        Cursor cursor = act.managedQuery( contentUri, proj, null, null, null );
        int column_index = cursor.getColumnIndexOrThrow( MediaStore.Images.Media.DATA );
        cursor.moveToFirst();
        return cursor.getString( column_index );
    }

    public static int gcd( int a, int b )
    {
        while ( b > 0 )
        {
            int temp = b;
            b = a % b; // % is remainder
            a = temp;
        }
        return a;
    }


    public static String facebookProfilePicForId( String userId, int width, int height )
    {
        return "http://graph.facebook.com/" + userId + "/picture?width=" + Integer.toString( width )
                + "&height=" + Integer.toString( height );
    }

    public static < T > Object jsonToObject( String inputJSON, Type T )
    {

        if ( inputJSON == null || inputJSON.equals( "" ) )
        {
            return null;
        }

        Gson gson = new Gson();
        Log.v( TAG, "Decoding " + T.toString() + " from JSON: " + inputJSON );
        Object obj = null;
        try
        {
            obj = gson.fromJson( inputJSON, T );
        }
        catch ( JsonParseException e )
        {
            Log.e( TAG, "Decoding JSON failed, inputJSON was: " + inputJSON, e );
        }
        catch ( IllegalStateException e )
        {
            Log.e( TAG, "Decoding JSON failed, inputJSON was: " + inputJSON, e );
        }
        return obj;
    }

    public static ImageLoader getConfiguredImageLoader( Context ctx )
    {
        ImageLoader imageLoader = ImageLoader.getInstance();
        if ( !imageLoader.isInited() )
        {
            DisplayImageOptions defaultOptions = getDisplayOptions();
            ImageLoaderConfiguration config =
                    new ImageLoaderConfiguration.Builder( ctx.getApplicationContext() )
                            .defaultDisplayImageOptions( defaultOptions )
                            .memoryCache( new UsingFreqLimitedMemoryCache( 20 * 1024 * 1024 ) )
                            .build();
            imageLoader.init( config );
        }
        return imageLoader;
    }

    public static DisplayImageOptions getDisplayOptions()
    {
        return new DisplayImageOptions.Builder().cacheInMemory( true ).cacheOnDisc( true )
                .displayer( new SimpleBitmapDisplayer() ).build();
    }

    public static void setViewBackgroundWithoutResettingPadding( final View v,
            final int backgroundResId )
    {
        final int paddingBottom = v.getPaddingBottom(), paddingLeft = v.getPaddingLeft();
        final int paddingRight = v.getPaddingRight(), paddingTop = v.getPaddingTop();
        v.setBackgroundResource( backgroundResId );
        v.setPadding( paddingLeft, paddingTop, paddingRight, paddingBottom );
    }

    // TODO: WARNING! This closes the whole activity on the push of OK
    // This should be documented in the method name or better, not be included
    // in a util function
    public static void displayErrorAlert( final Activity act, final String title,
            final String message )
    {

        AlertDialog.Builder confirm = new AlertDialog.Builder( act );
        confirm.setTitle( title );
        confirm.setMessage( message );

        confirm.setNegativeButton(
                act.getString( android.R.string.ok ), new DialogInterface.OnClickListener()
                {
                    @Override
                    public void onClick( DialogInterface dialog, int which )
                    {
                        act.finish();
                    }
                } );

        confirm.show().show();
    }

    /**
     * Converts dp to pixels
     *
     * @param ctx
     * @param dp
     * @return
     */
    public static int dp2px( Context ctx, int dp )
    {
        final float scale = ctx.getResources().getDisplayMetrics().density;
        int px = ( int ) ( dp * scale + 0.5f );

        return px;
    }

    /**
     * This method converts device specific pixels to density independent
     * pixels.
     *
     * @param px
     *            A value in px (pixels) unit. Which we need to convert into db
     * @param context
     *            Context to get resources and device specific display metrics
     * @return A float value to represent dp equivalent to px value
     */
    public static float convertPixelsToDp( float px, Context context )
    {
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        float dp = px / ( metrics.densityDpi / 160f );
        return dp;
    }

    public static Map<String, String> referralStringToMap( String referralString )
    {
        Pattern equalsSeparator = Pattern.compile( "=" );
        Map<String, String> result = new HashMap< String, String >();
        if ( referralString != null && !referralString.equals( "" ) )
        {
            String[] keyValues = referralString.split( "&" );
            for ( String kV : keyValues )
            {
                String[] keyAndValue = equalsSeparator.split( kV );
                if ( keyAndValue.length == 2 && keyAndValue[0] != null && !keyAndValue[0].equals(""))
                {
                    result.put( keyAndValue[0], keyAndValue[1] );
                }
            }
        }
        return result;
    }


    public static Map< String, List< String >> getAuthenticatedHeaders( Context ctx, String signature, String date )
    {
        Map< String, List< String >> headers = new HashMap< String, List< String >>();
        headers.put( "x-amz-date", Beans.toList( date ) );
//        headers.put( "Authorization", makeTranscoderAuthorization( signature ) );
        return headers;
    }

    public static byte[] getSHA256Hash( String password )
    {
        MessageDigest digest = null;
        try
        {
            digest = MessageDigest.getInstance( "SHA-256" );
        }
        catch ( NoSuchAlgorithmException e1 )
        {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }
        if ( digest != null )
        {
            digest.reset();
            return digest.digest( password.getBytes() );
        }
        return null;

    }

    public static String bin2hex( byte[] data )
    {
        if ( data != null)
        {
            return String.format( "%0" + ( data.length * 2 ) + "X", new BigInteger( 1, data ) ).toLowerCase();
        }
        return null;
    }

//    private static List<String> makeTranscoderAuthorization( String signature )
//    {
//        String auth = "AWS " + Constants.AWS_ACCESS_KEY_ID + ":" + signature;
//        return  Beans.toList( auth );
//    }
//
//    // This method converts AWSSecretKey into crypto instance.
//    public static Mac setKey(String AWSSecretKey) throws Exception
//    {
//      Mac mac = Mac.getInstance("HmacSHA1");
//      byte[] keyBytes = AWSSecretKey.getBytes("UTF8");
//      SecretKeySpec signingKey = new SecretKeySpec(keyBytes, "HmacSHA1");
//      mac.init(signingKey);
//      return mac;
//    }

//    // This method creates S3 signature for a given String.
//    public static String sign(String data, Mac mac) throws Exception
//    {
//      // Signed String must be BASE64 encoded.
//      byte[] signBytes = mac.doFinal(data.getBytes("UTF8"));
//      String signature = Base64.encodeToString( signBytes, Base64.NO_WRAP );
//      return signature;
//    }

    public static byte[] HmacSHA256(String data, byte[] key) throws Exception  {
        String algorithm="HmacSHA256";
        Mac mac = Mac.getInstance(algorithm);
        mac.init(new SecretKeySpec(key, algorithm));
        return mac.doFinal(data.getBytes("UTF8"));
   }

   public static byte[] getSignatureKey(String key, String dateStamp, String regionName, String serviceName) throws Exception  {
        byte[] kSecret = ("AWS4" + key).getBytes("UTF8");
        byte[] kDate    = HmacSHA256(dateStamp, kSecret);
        byte[] kRegion  = HmacSHA256(regionName, kDate);
        byte[] kService = HmacSHA256(serviceName, kRegion);
        byte[] kSigning = HmacSHA256("aws4_request", kService);
        return kSigning;
   }

}
