package com.projecta.hackathon.zalando.utils;

import java.util.Locale;

import android.content.Context;
import android.telephony.TelephonyManager;

public class CountryUtils
{

    public static Locale getLocale( Context ctx )
    {
        return ctx.getResources().getConfiguration().locale;
    }


    public static String getSimCountry( Context ctx )
    {
        TelephonyManager telService =
                ( TelephonyManager ) ctx.getSystemService( Context.TELEPHONY_SERVICE );
        String networkCountryIso = telService.getNetworkCountryIso();
        return networkCountryIso;
    }

}
